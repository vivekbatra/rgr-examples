import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EvenOddTest 
{
	
	
//	R1: Accepts 1 integer value, n
//	N > 1  
//	R2: If N < 1, then return false
//	R3: If n is even, return true
//	R4: If n is odd, return false
	EvenOdd e;
	
	@Before
	public void setUp() throws Exception {
		 e = new EvenOdd();
	}

	@After
	public void tearDown() throws Exception {
	}
	
//	R2: If N < 1, then return false
	@Test
	public void testNLessThan1() {
		int n = -999;
		assertEquals(false, e.isEven(n));
	}
	
//	R3: If n is even, return true
	@Test
	public void testNIsEven() {
		int n = 30;
		assertEquals(true, e.isEven(n));
	}
	
//	R4: If n is odd, return false
	@Test
	public void testNIsOdd() {
		int n = 35;
		assertEquals(false, e.isEven(n));
	}

}
