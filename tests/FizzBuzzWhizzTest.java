import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzWhizzTest 
{
	FizzBuzzWhizz ob;
	@Before
	public void setUp() throws Exception 
	{
		ob = new FizzBuzzWhizz(); 
	}
	
//	R0: Function must accept a number > 0. Number less than 0 return “error”
	@Test
	public void testIfGreaterThan0() 
	{
		assertEquals("error", ob.fizzOrBuzz(-1));
	}
	
	
	
//	R1: Return "fizz" if the number is divisible by 3
	@Test
	public void testIfdivisibleBy3() 
	{
		assertEquals("fizz", ob.fizzOrBuzz(9));
	}
	
	
//	R2:Return "buzz" if the number is divisible by 5
	@Test
	public void testIfdivisibleBy5() 
	{
		assertEquals("buzz", ob.fizzOrBuzz(10));
	}
	
//	R3: Return "fizzbuzz" if the number is divisible by 3 or 5 (15)
	@Test
	public void testIfdivisibleBy3And5() 
	{
		assertEquals("fizzbuzz", ob.fizzOrBuzz(15));
	}
//	R4: Return the number if no other requirement is fulfilled. The numbers must be returned as a string.
	@Test
	public void testIfNoConditionIsTrue() 
	{
		int num = 4;
		String numAsString = String.valueOf(num);
		assertEquals(numAsString, ob.fizzOrBuzz(num));
	}
	
//	R5: If number is prime, return “whizz” 
	@Test
	public void testIfPrime() 
	{
		assertEquals("whizz", ob.fizzOrBuzz(7));
	}
//	R6 If number meets (R5) AND (R1, R2, or R3) - append “whizz” to end of string
	@Test
	public void testIfAnswerIsCrazy() 
	{
		int num = 5;
		String expectedOutput = null;
		if (num % 3 == 0)
		{
			expectedOutput = "fizz";
		}
		else if (num % 5 == 0)
		{
			expectedOutput = "buzz";
		}
		assertEquals(expectedOutput + "whizz", ob.fizzOrBuzz(num));
	}
	
	
//	Example: 
//			1 returns “1”
//			2 returns “whizz”
//			3 returns “fizzwhizz”
//			4 returns “4”
//			5 returns “buzzwhizz”
								//	******* 6 returns “6” ***********
//			7 returns “whizz”
//			9 returns “fizz”
//			10 returns “buzz”
//			15 returns “fizzbuzz”
	

}

